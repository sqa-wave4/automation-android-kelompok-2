import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementExist(findTestObject('Beranda/btn_cat_Elektronik'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/btn_cat_HpAksesoris'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/btn_cat_KomputerAksesoris'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/btn_cat_PakaianPria'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/btn_cat_Semua'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/btn_cat_SepatuPria'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/btn_iconAddProduct'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/navBar_Akun'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/navBar_Beranda'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/navBar_Notifikasi'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/navBar_Transaksi'), 5)

Mobile.verifyElementExist(findTestObject('Beranda/txt_TelusuriKategori'), 5)

