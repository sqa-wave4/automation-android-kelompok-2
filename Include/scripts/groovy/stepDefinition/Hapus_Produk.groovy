package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Hapus_Produk {
	
	@Given("User goes to the my sell list page")
	public void user_goes_to_the_my_sell_list_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Diminati/Navigate to Akun'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("User want to go to the Produk page")
	public void user_want_to_go_to_the_Produk_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Semua Produk/Navigate to Daftar Jual Saya Page - Hapus'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("User click icon Delete")
	public void user_click_icon_Delete() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Semua Produk/Tap button Hapus'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User tap button Hapus for confirmation action")
	public void user_tap_button_Hapus_for_confirmation_action() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Semua Produk/Tap button Hapus - Konfirmasi Hapus Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
