package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Negotiation {
	@Given("user choose the product")
	public void user_choose_the_product() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Verify Product'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Tap Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("detail product is displayed")
	public void detail_product_is_displayed() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Verify Product detail displayed'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("tap button SayaTertarikNego")
	public void tap_button_SayaTertarikNego() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Tap button SayaTertarikNego'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Verify Halaman Nego is displayed'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("do not input price")
	public void do_not_input_price() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Input without price'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("tap button Kirim")
	public void tap_button_Kirim() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Tap button Kirim'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("error message is displayed")
	public void error_message_is_displayed() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Verify element error message'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("input price")
	public void input_price() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Input with price'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("success message is displayed")
	public void success_message_is_displayed() {
		Mobile.callTestCase(findTestCase('Pages/Page Negotiation/Verify element success message'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}