package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import cucumber.api.java.en.And
import internal.GlobalVariable

public class Profile {

	@Given("User on dashboard")
	public void user_on_dashboard() {
	}

	@When("User tap button profile")
	public void user_tap_button_profile() {

		Mobile.callTestCase(findTestCase('Pages/Profile/Tap Button Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User tap button edit")
	public void user_tap_button_edit() {

		Mobile.callTestCase(findTestCase('Pages/Profile/Verify Element Akun Saya'), [:], FailureHandling.STOP_ON_FAILURE)

		Mobile.callTestCase(findTestCase('Pages/Profile/Tap Button Edit Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User tap textview nama")
	public void user_tap_textview_nama() {

		Mobile.callTestCase(findTestCase('Pages/Profile/Verify Element Lengkapi Info Akun'), [:], FailureHandling.STOP_ON_FAILURE)

		Mobile.callTestCase(findTestCase('Pages/Profile/Click ViewGroup Nama'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User update nama")
	public void user_update_nama() {

		Mobile.callTestCase(findTestCase('Pages/Profile/Verify Element Card Nama'), [:], FailureHandling.STOP_ON_FAILURE)

		Mobile.callTestCase(findTestCase('Pages/Profile/Update Nama'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User tap textview password")
	public void user_tap_textview_password() {
		Mobile.callTestCase(findTestCase('Pages/Profile/Click ViewGroup Pwd'), [:], FailureHandling.STOP_ON_FAILURE)
	}


	@And("User update new password")
	public void user_update_new_password() {

		Mobile.callTestCase(findTestCase('Pages/Profile/Verify Element Card Ubah Password'), [:], FailureHandling.STOP_ON_FAILURE)

		Mobile.callTestCase(findTestCase('Pages/Profile/Update Password'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User success update the profile")
	public void user_success_update_the_profile() {
	}
}
