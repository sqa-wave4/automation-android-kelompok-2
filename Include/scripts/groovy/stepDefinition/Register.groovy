package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Register {

	@When("User click link daftar")
	public void user_click_link_daftar() {
		Mobile.callTestCase(findTestCase('Pages/Page Masuk/Tap Link Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.callTestCase(findTestCase('Pages/Page Daftar/Verify Element Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input nama lengkap")
	public void user_input_nama_lengkap() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar/Input Nama Lengkap - Register'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input email")
	public void user_input_email() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar/Input Random Email - Register'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input password")
	public void user_input_password() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar/Input Password - Register'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input NomorHP")
	public void user_input_NomorHP() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar/Input NomorHP - Register'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input Kota")
	public void user_input_Kota() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar/Input Kota - Register'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.callTestCase(findTestCase('Pages/Page Beranda/Swipe Down'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.callTestCase(findTestCase('Pages/Page Daftar/Verify Element Daftar after swipe down'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input Alamat")
	public void user_input_Alamat() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar/Input Alamat - Register'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click button daftar")
	public void user_click_button_daftar() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar/Tap Button Daftar - Register'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input registered email register")
	public void user_input_registered_email_register() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar/Input Email - Register'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Register success")
	public void register_success() {
		Mobile.callTestCase(findTestCase('Pages/Page Akun/Verify Element Akun - after login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Register failed")
	public void register_failed() {
		Mobile.callTestCase(findTestCase('Pages/Page Akun/Verify Element Akun - failed login'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
