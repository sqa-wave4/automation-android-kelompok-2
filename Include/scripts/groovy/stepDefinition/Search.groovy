package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Search {
	@Given("user access application")
	public void user_access_application() {
		Mobile.waitForElementPresent(findTestObject('Beranda/navBar_Beranda'), 3)
		Mobile.tap(findTestObject('Beranda/navBar_Beranda'), 0)
	}

	@When("Semua button is displayed on Beranda")
	public void semua_button_is_displayed_on_Beranda() {
		Mobile.verifyElementVisible(findTestObject('Page_Search/btn_Semua'), 0)
	}

	@Then("user click Semua button")
	public void user_click_Semua_button() {
		Mobile.tap(findTestObject('Page_Search/btn_Semua'), 0)
	}

	@Then("product list by category is displayed")
	public void product_list_by_category_is_displayed() {
		Mobile.verifyElementVisible(findTestObject('Page_Search/productlist_category'), 0)
	}

	@When("Elektronik button is displayed on Beranda")
	public void elektronik_button_is_displayed_on_Beranda() {
		Mobile.verifyElementVisible(findTestObject('Page_Search/btn_Elektronik'), 0)
	}

	@Then("user click Elektronik button")
	public void user_click_Elektronik_button() {
		Mobile.tap(findTestObject('Page_Search/btn_Elektronik'), 0)
	}

	@When("ElektronikAksesoris button is displayed on Beranda")
	public void elektronikaksesoris_button_is_displayed_on_Beranda() {
		Mobile.verifyElementVisible(findTestObject('Page_Search/btn_KomputerdanAksesoris'), 0)
	}

	@Then("user click ElektronikAksesoris button")
	public void user_click_ElektronikAksesoris_button() {
		Mobile.tap(findTestObject('Page_Search/btn_KomputerdanAksesoris'), 0)
	}

	@When("Search field is displayed on Beranda")
	public void search_field_is_displayed_on_Beranda() {
		Mobile.verifyElementVisible(findTestObject('Page_Search/field_searchBeranda'), 0)
	}

	@Then("user click Search field")
	public void user_click_Search_field() {
		Mobile.tap(findTestObject('Page_Search/field_searchBeranda'), 0)
		Mobile.verifyElementVisible(findTestObject('Page_Search/field_inputproduct'), 0)
	}

	@Then("product list by productname is displayed")
	public void product_list_by_productname_is_displayed() {
		Mobile.verifyElementVisible(findTestObject('Page_Search/productlist_name'), 0)
	}
}