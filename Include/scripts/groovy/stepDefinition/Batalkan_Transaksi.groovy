package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Batalkan_Transaksi {

	@Given("User wants to go Daftar Jual Saya page from Akun")
	public void user_wants_to_go_Daftar_Jual_Saya_page_from_Akun() {
		Mobile.callTestCase(findTestCase('Pages/Page Diminati/Navigate to Akun'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User is in Daftar Jual Saya page")
	public void user_is_in_Daftar_Jual_Saya_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Navigate to Daftar Jual Saya Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User stay in Diminati page")
	public void user_stay_in_Diminati_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Navigate to Diminati Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User come to Detail Product page")
	public void user_come_to_Detail_Product_page() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Go to the Detail Produk - Batalkan Transaksi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click button Terima")
	public void user_click_button_Terima() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Tap button Terima'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User drag popup hubungi wa")
	public void user_drag_popup_hubungi_wa() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Drag Popup Hubungi WA'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User tap button Status")
	public void user_tap_button_Status() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Tap button Status'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User click button Batalkan Transaksi")
	public void user_click_button_Batalkan_Transaksi() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Tap button Batalkan Transaksi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User tap Simpan button")
	public void user_tap_Simpan_button() {
		Mobile.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Tap button Simpan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
