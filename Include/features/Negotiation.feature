@Negotiation
Feature: Negotiation
  User want to negotiation product

  @NEG001
  Scenario: user want to negotiation product without input price
    Given user choose the product
    When detail product is displayed
    Then tap button SayaTertarikNego
    Then do not input price
    Then tap button Kirim
    Then error message is displayed
    
  @NEG002
  Scenario: user want to negotiation product with input price
    Given user choose the product
    When detail product is displayed
    Then tap button SayaTertarikNego
    Then input price
    Then tap button Kirim
    Then success message is displayed
    
