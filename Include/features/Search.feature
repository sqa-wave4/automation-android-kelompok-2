@Search
Feature: Search
  user want to search product

  @SCR001
  Scenario: user want to search product by Semua category
    Given user access application
    When Semua button is displayed on Beranda
    Then user click Semua button
    Then product list by category is displayed

  @SCR002
  Scenario: user want to search product by Elektronik category
    Given user access application
    When Elektronik button is displayed on Beranda
    Then user click Elektronik button
    Then product list by category is displayed
    
  @SCR003
  Scenario: user want to search product by ElektronikAksesoris category
    Given user access application
    When ElektronikAksesoris button is displayed on Beranda
    Then user click ElektronikAksesoris button
    Then product list by category is displayed
    
  @SCR004
  Scenario: user want to search product by product name
    Given user access application
    When Search field is displayed on Beranda
    Then user click Search field
    Then product list by productname is displayed