@Register
Feature: Register

  @RGI001
  Scenario: User want to register with valid credential
    Given User already on login page
    When User click link daftar
    And User input nama lengkap
    And User input email
    And User input password
    And User input NomorHP
    And User input Kota
    And User input Alamat
    And User click button daftar
    Then Register success

  @RGI002
  Scenario: User want to register with registered email
    Given User already on login page
    When User click link daftar
    And User input nama lengkap
    And User input registered email register
    And User input password
    And User input NomorHP
    And User input Kota
    And User input Alamat
    And User click button daftar
    Then Register failed
