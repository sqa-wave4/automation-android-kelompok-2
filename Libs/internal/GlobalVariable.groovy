package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p>Profile default : Binar Mobile Application</p>
     */
    public static Object id_app_binar
     
    /**
     * <p>Profile default : This e-mail registered on Binar's App</p>
     */
    public static Object email
     
    /**
     * <p>Profile default : This is the password for sub.issadha@gmail.com</p>
     */
    public static Object password
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += TestCaseMain.getParsedValues(RunConfiguration.getOverridingParameters())
    
            id_app_binar = selectedVariables['id_app_binar']
            email = selectedVariables['email']
            password = selectedVariables['password']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
